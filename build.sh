#!/usr/bin/env /bin/sh

gcc -std=c2x -Werror -Wall -Wextra -Wshadow -pedantic -fsanitize=address main.c -o main
