#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

struct Base;
struct Child;
struct Child2;

typedef struct Base Base_t;
typedef struct Child Child_t;
typedef struct Child2 Child2_t;

enum TypeTag
{
  BASE = 0,
  CHILD,
  CHILD2
};

typedef enum TypeTag TypeTag_t;

struct vtable
{
  union 
  {
    void *actual_ptr;
    Child_t *c;
    Child2_t *c2;
  } vptr;
  TypeTag_t tag;
};

typedef struct vtable vtable_t;

typedef void(*print)(Base_t *);

struct Base
{
  vtable_t vtable;
  int k;
  print p;
};

struct Child
{
  struct Base base;
  int a;
  int b;
};

struct Child2
{
  struct Base base;
  float c;
};

void BasePrintImpl(Base_t *b)
{
  printf("Base %d\n", b->k);
}

void ChildPrint(Child_t *c);
void Child2Print(Child2_t *c2);

void BasePrint(Base_t *b)
{
  switch(b->vtable.tag)
  {
    case BASE:
       BasePrintImpl(b);
       break;
    case CHILD:
       ChildPrint(b->vtable.vptr.c);
       break;
    case CHILD2:
       Child2Print(b->vtable.vptr.c2);
       break;
  }
}

Base_t BaseConstructor(void* child_ptr, TypeTag_t tag, int k)
{
  print print_ptr = BasePrint;

  if(child_ptr == NULL) tag = BASE;
  Base_t base;
  base.vtable.vptr.actual_ptr = child_ptr;
  base.vtable.tag = tag;
  base.k = k;
  base.p = print_ptr;
  return base;
}

void ChildPrint(Child_t *c)
{
  printf("Child %d %d %d\n", c->base.k, c->a, c->b);
}

Child_t ChildConstructor(int k, int a, int b)
{
  static const TypeTag_t tag = CHILD;
  Child_t *this = (Child_t *) malloc(sizeof(Child_t));
  this->a = a;
  this->b = b;
  this->base = BaseConstructor(this, tag, k);
  return *this;
}

void Child2Print(Child2_t *c2)
{
  printf("Child2 %d %f\n", c2->base.k, c2->c);
}

Child2_t Child2Constructor(int k, float c)
{
  static const TypeTag_t tag = CHILD2;
  Child2_t *this = (Child2_t *) malloc(sizeof(Child2_t));
  this->c = c;
  this->base = BaseConstructor(this, tag, k);
  return *this;
}

int func(Base_t * ptr)
{
  ptr->p(ptr);
  return 0;
}

int main(int argc, char *argv[])
{
  (void)argc;
  (void)argv;
  Child_t c = ChildConstructor(2, 3, 4);
  Base_t *ptr = (Base_t *)&c;
  func(ptr);

  Child2_t c2 = Child2Constructor(6, 3.14);
  ptr = (Base_t *)&c2;
  func(ptr);

  Base_t b = BaseConstructor(NULL, 0, 283);
  func(&b);

  free(c.base.vtable.vptr.c);
  free(c2.base.vtable.vptr.c2);

  return 0;
}
